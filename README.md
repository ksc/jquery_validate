jquery 表单验证插件
=======
可方便的 验证 汉字、数字类型、QQ、email格式 、密码长度、密码是否一致、数字大小
验证规则填写在 所需验证条目的 validate 属性中，比如

		<input type="text" name="pwd" id="pwd"  validate="notnull|minLength:5|maxLength:18">限制密码长度在5-18 
        <input type="text" name="pwd2" id="pwd2"  validate="notnull|samewith:pwd2">限制必须与pwd一致

具体使用方法见demo