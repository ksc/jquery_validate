/*!
 * jQuery validate Plugin 1.2
 * Copyright 2011, ksc
 * last update: 2013.10.15
 */
jQuery.extend({
	validate:function(f_id,opts){
		var def ={
	    	// 'elementSupport':":text, :password, textarea, select",
			 'event'		 :'change',//change,blur,now
			 'get_label'     : function (){ return '此项'; },
			 'error_msg'    : error_msg,
			 'onerror'		: function(){},//当验证某一项失败是调用
			 'success_msg'  : function(id,msg){this.error_msg(id,'',false);},
			 'ext'          : {'_id':function(id){}}	//扩展验证
		};	
		
		var opt=$.extend({},def,opts);
		var ext=opt.ext;	
		var patterns={
			email	:"^\\w+@\\w+(?:\\.[A-Za-z]{2,5}){1,2}$",
			tel		:"^[0-9|\\-| ]{5,15}$",
			qq		:"^[0-9]{5,12}$",
			only_w	:"^\\w+$",
			num		:"^[\\d|\\.|,]+$",
			int		:"^\\d"
			
			};

	
		var v_obj=$('#'+f_id)[0];
		if(v_obj.nodeName=='FORM'){ 
			var $list=$(v_obj).find('input[validate],textarea[validate],select[validate]');
			$list.each(function(){//为每个需要验证的元素初始化label
				this.n=opt.get_label(this);
			});
			$list.bind(opt.event,function(){
 				if(validate_it(this)){
					 if($.isFunction(ext[this.id])){
				 	 	ext[this.id](this.value,opt);
					 }	
				}
			});
			$(v_obj).submit(function(){//表单提交检查
			 var can=false;
			  $list.each(function(){
				can=validate_it(this);

				return can;
			  });
			 return can;
			});
		}//end IF FORM
		function validate_it(el){//验证单个元素
			var m='', re;
		 
			 var v=$(el).attr('validate');
			
			 if(v.indexOf('link')>=0){
				 var t=getV('link',v);
				return validate_it($('#'+t)[0]);
				 
			 }
			 if(check('trim',v)){ //不能有空格
 					 el.value=$.trim(el.value);
			 }
			 var value=el.value;
			 
	 
			 if(check('notnull',v) ||check('require',v)){//is null
			 	if(el.tagName=='SELECT'){
					m=(value==getV('default',v))?'必须选择':'';//如果还是默认值，即没有选择
				}
				else if(el.type=='file'){
					m=(value=='')?'必须选择':'';	
				}
			    else if(value.length==0){// INPUT
			 		m='不能为空'; 
					 
 				}else{
				 
				}
		 
			 }
			 if(el.type=='file' && v!='' && m==''){
				var limit=getV('fileLimit',v);
				var filename=value.split('\\').pop();
 				var f_ext=filename.split('.').pop();
				if(limit.indexOf(f_ext)==-1){
					m='文件类型必须是'+limit+'中的一种';
				}
  			 }
			if(check('samewith',v) && m==''){//是否与某一项的值一致
  					var t1=$('#'+getV('samewith',v))[0];
					if(value!=$(t1).val()){
 						m='与'+opt.get_label(t1)+'不一致';
					}	
			}
			if(check('minVal',v) && m==''){
				var min_v=parseInt(getV('min',v));
				m=value.value<min_v?'太小了':''; 
			}
			if(check('minLength',v) && m=='' && value!=''){//只有在 前几步检查不出错，并且该项值不为空时才 检查
				if(value.length<getV('minLength',v)){
					m='长度不符合要求';	
				}	
			}
			if(check('maxLength',v) && m=='' && value!=''){
				if(value.length>getV('maxLength',v)){
					m='长度不符合要求';	
				}	
			}
			if(m==''){
                for(key in patterns){
                    if(check(key,v)){
                        re=new RegExp(patterns[key],'g');
                        if(!re.test(value)){
                            m='格式不对';
                            break;
                        }
                    }	 
                     
                }//end for
			}
		 
			if(m.length>0){
				opt.error_msg(el.id,el.n+m,true);
				 
				return false;	
			}

			opt.success_msg(el.id,'',false);
			return true;
            
		}//end validate_it
		function getV(key,v){
			var re=new RegExp(key+":([\\w,]+)");
			try{return v.match(re)[1];}catch(e){ alert(key+'的值为空，请检查')};
		}
        /*
         * 默认处理验证结果的函数
         * msg 为空的时候 验证成功
         */
		function error_msg(id,msg,setfocus){
			if(setfocus){  
				setTimeout(function(){ $('#'+id).focus();},100);
			}
 			$error=$('#'+id).next('.error_msg');
			if($error.length>0){
				$error.html(msg).show();	
			}else{
				$('#'+id).after('<label for='+id+' class="error_msg" >'+msg+'</label>');	
			}

		}
		function check(item,v){
			//var re =new RegExp(item+'([\\|:][\\w]+)?$');
			var re =new RegExp(item);
			return re.test(v);
		}

	return this;

	}
});